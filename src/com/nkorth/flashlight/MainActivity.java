package com.nkorth.flashlight;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.view.WindowManager;

public class MainActivity extends Activity {

	Camera camera;
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	@Override
	protected void onResume() {
		camera = Camera.open();
		Parameters params = camera.getParameters();
		params.setFlashMode(Parameters.FLASH_MODE_TORCH);
		camera.setParameters(params);
		camera.startPreview();
		super.onResume();
	}

	@Override
	protected void onPause() {
		Parameters params = camera.getParameters();
		params.setFlashMode(Parameters.FLASH_MODE_AUTO);
		camera.setParameters(params);
		camera.stopPreview();
		camera.release();
		super.onPause();
	}
	
}